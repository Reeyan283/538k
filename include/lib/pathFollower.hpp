#pragma once
#include "libs.hpp"

class myPoint {
  public:
    double x, y, distance, curvature, velocity;

    myPoint() {}
    myPoint(double x, double y): x(x), y(y) {}
    myPoint(double x, double y, double distance, double curvature, double velocity): x(x), y(y), distance(distance), curvature(curvature), velocity(velocity) {}
};

class PathFollower {
  private:
    double rate_max;
    double wheelTrack;
    double x, y, theta;
    double straightLookaheadDistance;
    double lookaheadDistance;
    double lookaheadCurvature;
    double targetVelocity;
    int closestPoint;
    double lookaheadIndex;
    int counter;
    std::vector<double> lookaheadPoint;
    std::vector<myPoint> currentPath;
    bool finished;

    void getClosestPoint();
    void getLookahead();
    void getLookaheadCurvature();
  public:
    // dt in seconds, a_max in in/s/s, wheelTrack in in, straightLookaheadDistance in in
    PathFollower(double dt, double a_max, double wheelTrack, double straightLookaheadDistance);
    void setPosition(double x, double y, double theta);
    void setPath(std::vector<myPoint> path);
    std::vector<double> getTargetVelocities();
    bool isSettled();
    double getClosestPointIndex();
    std::vector<double> getLookaheadPoint();
};
