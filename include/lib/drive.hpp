#pragma once
#include "libs.hpp"

class Drive {
    // private:


    public:
        okapi::Motor FLMotor, BLMotor, FRMotor, BRMotor;
        Drive(int FLPort, int BLPort, int FRPort, int BRPort,
              bool FLRev, bool BLRev, bool FRRev, bool BRRev,
              okapi::AbstractMotor::gearset gearset,
              okapi::AbstractMotor::brakeMode brakeMode,
              okapi::AbstractMotor::encoderUnits encoderUnits);

        void tank_velocity(okapi::Controller controller, int maxVel);
        void tank_voltage(okapi::Controller controller, int maxVol);
        void arcade_velocity(okapi::Controller controller, int maxVel);
        void arcade_voltage(okapi::Controller controller, int maxVol);
        void x_velocity(okapi::Controller controller, int maxVel);
        void x_voltage(okapi::Controller controller, int maxVol);

        void move_velocity(int lVel, int rVel);
        void moveX_voltage(int flVol, int frVol, int blVol, int brVol);
        void move_voltage(int lVol, int rVol);
        void set_brake_mode(okapi::AbstractMotor::brakeMode);

        std::vector<std::vector<double>> telemetry();
};
