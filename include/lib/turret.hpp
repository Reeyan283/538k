#pragma once
#include "libs.hpp"
#include "odom.hpp"
#include "pid.hpp"

class Turret {
private:
    pros::Vision& camera;
    okapi::Potentiometer& pot;
    Odom& odom;
    PID& pid;
    okapi::Motor& motor;
    Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> T_rt_0;
    Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> T_tc;
    Eigen::Vector3d v_wg;
    bool red;
    int potZero;
    double potAdjust;
    std::atomic<bool> turretEnabled;

    pros::Mutex v_wgMutex;

public:
    /**
     * @brief Construct a new Turret object
     * 
     * @param camera vision sensor attached to turret (must turn with turret)
     * @param pot potentiometer to measure turret position
     * @param potZero potentiometer readout when turret is aligned with drivetrain (0 to 4095)
     * @param potAdjust coefficient to adjust for potentiometer scaling
     * @param odom Odom object to measure robot position
     * @param pid PID object to move turret
     * @param motor Motor to move turret
     * @param color goal to look for ("RED" or "BLUE")
     * @param xTurretOffset right-positive coordinate of turret center from tracking center
     * @param yTurretOffset forward-positive coordinate of turret center from tracking center
     * @param zTurretOffset up-positive coordinate of turret center from tracking center
     * @param thetaTurretOffset right-hand-rule negative rotation of turret center from tracking center
     * @param phiTurretOffset right-hand-rule positive rotation of turret center from tracking center
     * @param xCameraOffset right-positive coordinate of camera from turret center
     * @param yCameraOffset forward-positive coordinate of camera from turret center
     * @param zCameraOffset up-positive coordinate of camera from turret center
     * @param thetaCameraOffset right-hand-rule negative rotation of camera from turret center
     * @param phiCameraOffset right-hand-rule positive rotation of camera from turret center
     */
    Turret(pros::Vision& camera, okapi::Potentiometer& pot, int potZero, double potAdjust, Odom& odom, PID& pid, okapi::Motor& motor, std::string color,
        double xTurretOffset, double yTurretOffset, double zTurretOffset, double thetaTurretOffset, double phiTurretOffset,
        double xCameraOffset, double yCameraOffset, double zCameraOffset, double thetaCameraOffset, double phiCameraOffset);

    /**
     * @brief Get the 4x4 affine transformation of the robot frame in the world frame, changes as the robot moves
     * 
     * @return Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> a 3d transformation representing the robot frame in the world frame
     */
    Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> getT_wr();

    /**
     * @brief Get the 4x4 affine transformation of the turret frame in the robot frame, changes as the turret turns
     * 
     * @return Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> a 3d transformation representing the turret frame in the robot frame
     */
    Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> getT_rt();

    /**
     * @brief Get the 4x4 affine transformation of the camera frame in the turret frame
     * 
     * @return Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> a 3d transformation representing the camera frame in the turret frame
     */
    Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> getT_tc();
    
    /**
     * @brief Get the v_wg object representing the position of the goal in the world frame
     * 
     * @return Eigen::Vector3d the position of the goal in the world frame
     */
    Eigen::Vector3d getv_wg();

    /**
     * @brief Checks if the turret is eneabled
     * 
     * @return true turret is enabled
     * @return false turret is disabled
     */
    bool getTurretEnabled();

    /**
     * @brief Reads objects from the camera into the given array
     * 
     * @param size_id the size index to start from where 0 is the largest, excludes all objects before size_id
     * @param sig_id the color signature to look for
     * @param object_count the number of objects to read in
     * @param object_arr the array to read into
     */
    void readBySig(const std::uint32_t size_id, const std::uint32_t sig_id, const std::uint32_t object_count, pros::vision_object_s_t *const object_arr);

    double getError();

    bool goalInTurretRange();

    /**
     * @brief moves the stored goal position
     * 
     * @param adjustment the vector to be added to the goal position vector
     */
    void adjustGoal(Eigen::Vector3d adjustment);

    /**
     * @brief Enables or disables the turret
     * 
     * @param state the state to set the turret to
     */
    void setTurretEnabled(bool state);

    /**
     * @brief moves the turret motor at the given voltage
     * 
     * @param vol the voltage to run the motor at
     */
    void turnVoltage(int vol);

private:
    /**
     * @brief Should not be used outside of the constructor, loop method to aim the turret at the goal
     * 
     * @param arg the turret object (this)
     */
    static void aim(void* arg);

    /**
     * @brief Shoudl not be used outside of the constructor, loop method to adjust the goal position using the camera
     * 
     * @param arg the turret object (this)
     */
    static void adjustGoalPos(void* arg);

    /**
     * @brief deprecated code for reference
     * 
     */
    // Frame getf_wc();
    // Eigen::Vector3d getv_cg();
    // Eigen::Vector4i getBox(Eigen::Vector3d cg);
    // Eigen::Vector2i getSize(Eigen::Vector3d cg);
    // void updateGoalPos();
    // Eigen::Vector3d getv_wg();
    // bool goalInFrame(Eigen::Vector3d cg);
    // Eigen::Vector3d getv_rg();
    // void setTurretState(bool state);
    // static void pidFunction(void* m);
    // bool getTurretState();
    // PID getPID();
};
