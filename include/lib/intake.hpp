#pragma once
#include "libs.hpp"

class Intake {
    private:
        okapi::ControllerButton& fBtn, rBtn, rlBtn;
        int running;
        std::atomic<bool> enabled, overriding;
        
        // state indicator: -3(match auton roller) -2(roller) -1(reverse) 0(stopped) 1(forward) 2(roller reversed) 
        std::atomic<int> state;

    public:
        okapi::Motor motorA, motorB;

        Intake(int motorA_port, int motorB_port, 
               bool motorA_rev, bool motorB_rev,
               okapi::AbstractMotor::gearset gearset,
               okapi::AbstractMotor::brakeMode brakeMode,
               okapi::AbstractMotor::encoderUnits encoderUnits,
               okapi::Controller controller,
               okapi::ControllerDigital forwardButton,
               okapi::ControllerDigital reverseButton,
               okapi::ControllerDigital rollerButton);
        
        void drive(okapi::Controller controller, int maxVol, int rolVel);

        void setBrakeMode(okapi::AbstractMotor::brakeMode brakeMode);
        void moveVoltage(int vol);    
        void moveVelocity(int vel);
        void setState(int state);
        int getState();
        void setEnabled(bool enable);
        bool getEnabled();
        double getTorque();
        static void run(void* args);
};
