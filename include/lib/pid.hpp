#pragma once
#include "libs.hpp"

class PID {
    private:
        double kP, kI, kD;
        double threshold;
        int timeoutCount, counter;
        double min, max;
        double DLimit, IThreshold;
        double i;
        double lastTarget, lastError;
        bool first;
    public:
        PID(double kP, double kI, double kD, double threshold, int timeoutCount, double IThreshold, double DLimit, double min, double max);
        void resetI();
        void resetD();
        double calculate(double target, double input);
        bool isFinished();
        double getError();
};
