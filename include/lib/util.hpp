#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <queue>
#include <deque>
#include <bitset>
#include <iterator>
#include <list>
#include <stack>
#include <map>
#include <set>
#include <functional>
#include <numeric>
#include <utility>
#include <limits>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <fstream>

#include "Eigen/Dense"

#include "api.h"
#include "okapi/api.hpp"
#include "okapi/impl/device/controller.hpp"
#include "okapi/impl/device/controllerUtil.hpp"
#include "pros/api_legacy.h"

/**
 * @brief A struct to represent a reference frame. Has a translation vector p and a rotation matrix R.
 * 
 * Modern Robotics, Lynch and Park, (3.16)
 */
struct Frame{
    Eigen::Vector3d p;
    Eigen::Matrix3d R;
};

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

#define sc static const
#define sce static constexpr
class Const{
public:
    /**
     * @brief motors
     * 
     */
    sc int DRIVE_FL_ID = 10; sc bool DRIVE_FL_REV = true;
    sc int DRIVE_FR_ID = 6; sc bool DRIVE_FR_REV = false;
    sc int DRIVE_BL_ID = 9; sc bool DRIVE_BL_REV = true;
    sc int DRIVE_BR_ID = 20; sc bool DRIVE_BR_REV = false;

    sc int INTAKE_A_ID = 17; sc bool INTAKE_A_REV = true;
    sc int INTAKE_B_ID = 7; sc bool INTAKE_B_REV = true;

    sc int FLY_ID = 1; sc bool FLY_REV = true;

    sc int VISION_ID = -1;


    /**
     * @brief 3-wires
     * 
     */
    sc char INDEXER_ID = 'A';


    /**
     * @brief odometry
     * 
     */
    sc int ODOM_L_ID = 5; sc bool ODOM_L_REV = false; sce double ODOM_L_DIA = 2.75*72/120;
    sc int ODOM_B_ID = 18; sc bool ODOM_B_REV = true; sce double ODOM_B_DIA = 2.75*72/120;
    sc int ODOM_IMU_ID = 4;
    sce double ODOM_L_OFFSET = 7.40625;
    sce double ODOM_R_OFFSET = 1.5625;
    sce double ODOM_B_OFFSET = 1.5625;


    /**
     * @brief controller buttons
     * 
     */
    sc auto INTAKE_FWD_BTN = okapi::ControllerDigital::R1;
    sc auto INTAKE_REV_BTN = okapi::ControllerDigital::X;
    sc auto INTAKE_RLR_BTN = okapi::ControllerDigital::L2;


    /**
     * @brief arc PID
     * 
     */
    sce double ARCDRIVE_PID_P = 1200;
    sce double ARCDRIVE_PID_I = 30;
    sce double ARCDRIVE_PID_D = 800;
    sce double ARCDRIVE_PID_ERROR_THRESHOLD = 1;
    sc int ARCDRIVE_PID_ERROR_TIMEOUT = 10;
    sce double ARCDRIVE_PID_I_THRESHOLD = 5;
    sce double ARCDRIVE_PID_D_LIMIT = 10000;
    sce double ARCDRIVE_PID_MAX_VOL = 12000;

    sce double ARCTURN_PID_P = 240000;
    sce double ARCTURN_PID_I = 0;
    sce double ARCTURN_PID_D = 0;
    sce double ARCTURN_PID_ERROR_THRESHOLD = 0.001;
    sc int ARCTURN_PID_ERROR_TIMEOUT = 10;
    sce double ARCTURN_PID_I_THRESHOLD = HUGE_VAL;
    sce double ARCTURN_PID_D_LIMIT = 10000;
    sce double ARCTURN_PID_MAX_VOL = 12000;


    /**
     * @brief point turn PID
     * 
     */
    sce double TURN_PID_P = 300;
    sce double TURN_PID_I = 25;
    sce double TURN_PID_D = 0;
    sce double TURN_PID_ERROR_THRESHOLD = 0.25;
    sc int TURN_PID_ERROR_TIMEOUT = 50;
    sce double TURN_PID_I_THRESHOLD = 5;
    sce double TURN_PID_D_LIMIT = 10000;
    sce double TURN_PID_MAX_VOL = 12000;


    /**
     * @brief intake speeds
     * 
     */
    sc int INTAKE_FWD_VOL = 12000;
    sc int INTAKE_RLR_VEL = 400;
    sc int INTAKE_AUTO_VOL = 7000;

    sce double VISION_DAMPENING_FACTOR = 0.01;


    sce double TURRET_LO_BOUND = -75;
    sce double TURRET_HI_BOUND = 75;
};
#undef sc
#undef sce
