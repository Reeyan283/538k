#pragma once
#include "lib/drive.hpp"
#include "lib/odom.hpp"
#include "lib/pathFollower.hpp"
#include "lib/file-reader.hpp"
#include "lib/pid.hpp"
#include "lib/turret.hpp"
#include "lib/intake.hpp"

extern Drive drive;
extern Odom odom;
extern Intake intake;

extern int running;
extern bool fBtn_pressed;
