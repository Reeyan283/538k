#pragma once
#include "libs.hpp"

class Odom {
    private:
        double x, y, theta;

        double offsetL, offsetR, offsetB;
        double thetaOffset;

        double lastL, lastR, lastB;
        double lastTheta;

        int counter;

        okapi::RotationSensor rotationSensorL, rotationSensorB;
        double lDiameter, bDiameter;
        pros::Imu IMU;

    public:
        Odom(
            double leftOffset, double rightOffset, double backOffset,
            okapi::RotationSensor RSL, double lDiameter,
            okapi::RotationSensor RSB, double bDiameter,
            pros::Imu compass
        );

        void set(double x_0, double y_0, double theta_0);

        double getX();
        double getY();
        double getTheta();

        double getRotL();
        double getRotB();
        double getIMU();

	void calibrateIMU();

        void update();
};
