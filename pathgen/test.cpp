#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

int main() {
  ifstream file("output.txt");
  vector<vector<double>> values {};
  string line;
  
  while(getline(file, line)) {
    double dtmp;
    vector<double> tmp;
    stringstream ss(line);
    while(ss >> dtmp) {
      string tmpstr;
      tmp.push_back(dtmp);
      if(!getline(ss, tmpstr, ',')) break;
    }
    values.push_back(tmp);
  }

  for(auto row: values) {
    for(auto col: row) printf("  %f", col);
    printf("\n");
  }
  return 0;
}