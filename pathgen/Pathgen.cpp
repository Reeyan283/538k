#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>

using namespace std;

// /**
//  * @brief data structure for a point on a path
//  * 
//  */
// class Point {
//     public:
//         double x, y, distance, curvature, velocity;

//         Point() {}
//         Point(double x, double y): x(x), y(y), distance(0), curvature(0), velocity(0) {}
//         Point(double x, double y, double distance, double curvature, double velocity): x(x), y(y), distance(distance), curvature(curvature), velocity(velocity) {}
// };

unsigned nChoosek( unsigned n, unsigned k )
{
    if (k > n) return 0;
    if (k * 2 > n) k = n-k;
    if (k == 0) return 1;

    int result = n;
    for( int i = 2; i <= k; ++i ) {
        result *= (n-i+1);
        result /= i;
    }
    return result;
}

double distance(double x_0, double y_0, double x_1, double y_1) {
    return sqrt(pow(x_1-x_0, 2) + pow(y_1-y_0, 2));
}

/**
 * @brief calculates a path
 * 
 * @param maxInterval the max acceptable distance between adjacent generated points in inches
 * @param k turning constant: affects max speed around turns
 * @param v_max robot's max speed in inches/second
 * @param a_max robot's max acceleration in inches/second^2
 * @param v_f desired final velocity in inches/second
 * @param n number of bezier control points
 * @param points array of 2d bezier control points
 * @return vector<Point> 
 */
vector<vector<double>> generatePath(double maxInterval, vector<vector<double>> points) {
    vector<vector<double>> result;
    int n = points.size();

    // find the longest distance between adjacent points and calculate how many points to generate
    double maxDistance = 0;
    for(int i=1; i<n; i++) {
        maxDistance = max(maxDistance, distance(points[i-1][0], points[i-1][1], points[i][0], points[i][1]));
    }
    double numPoints = ceil((n-1)*maxDistance/maxInterval);
    double tInterval = 1/numPoints;

    // calculate points using bezier
    for(double t=0; t<1; t+=tInterval) {
        double x = 0;
        double y = 0;
        for(int i=0; i<n; i++) {
            x += nChoosek(n-1, i)*pow(1-t, n-i-1)*pow(t, i)*points[i][0];
            y += nChoosek(n-1, i)*pow(1-t, n-i-1)*pow(t, i)*points[i][1];
        }
        result.push_back({x, y});
    }
    result.push_back({points[n-1][0], points[n-1][1]});

    // calculate distance for each point
    // double runningDistance = 0;
    // result[0].distance = 0;
    // for(int i=1; i<result.size(); i++) {
    //     runningDistance += distance(result[i-1].x, result[i-1].y, result[i].x, result[i].y);
    //     result[i].distance = runningDistance;
    // }

    // calculate curvature for each point
    result[0].push_back(0);
    for(int i=1; i<result.size()-1; i++) {
        double x_1 = result[i-1][0];
        double y_1 = result[i-1][1];
        double x_2 = result[i][0];
        double y_2 = result[i][1];
        double x_3 = result[i+1][0];
        double y_3 = result[i+1][1];

        if(x_1 == x_2) {
            x_1 += 0.0001;
        }

        double k_1 = 0.5*(pow(x_1, 2) + pow(y_1, 2) - pow(x_2, 2) - pow(y_2, 2)) / (x_1-x_2);
        double k_2 = (y_1-y_2)/(x_1-x_2);
        double b = 0.5*(pow(x_2, 2) - 2*x_2*k_1 + pow(y_2, 2) - pow(x_3, 2) + 2*x_3*k_1 - pow(y_3, 2)) / (x_3*k_2 - y_3 + y_2 - x_2*k_2);
        double a = k_1 - k_2*b;
        double r = sqrt(pow(x_1-a, 2) + pow(y_1-b, 2));
        result[i].push_back(1/r);
    }
    result[result.size()-1].push_back(0);

    // calculate velocity for each point
    // result[result.size()-1].velocity = v_f;
    // for(int i=result.size()-2; i>=0; i--) {
    //     double distance = fabs(result[i+1].distance - result[i].distance);
    //     result[i].velocity = min(min(v_max, k/result[i].curvature), sqrt(pow(result[i+1].velocity, 2) + 2*a_max*distance));
    // }

    return result;
}

int main() {
    // string outfile = "output.txt";
    vector<vector<double>> points = {{0, 0}, {0, 24}, {24, 24}};
    vector<vector<double>> test = generatePath(3, points);
    // ofstream myfile;
    // myfile.open (outfile.c_str(), ofstream::trunc);
    // for(int i=0; i<test.size(); i++) {
    //     myfile << fixed;
    //     myfile << test[i].x << "," << test[i].y << "," << test[i].distance << "," << test[i].curvature << "," << test[i].velocity << endl;
    // }
    // myfile.close();
    return 0;
}
