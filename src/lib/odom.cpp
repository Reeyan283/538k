#include "odom.hpp"
using namespace std;
/*
        ===             ===
         +               +
        ++---------------++
        |                 |
        |                 |
        |                 |--------------------------------> y
        |                 |
        |                 |
        ++---------------++
         +       |       +
        ===      |      ===
                 |
                 |
                 |
                 |
                 v
                 x
*/

Odom::Odom (
    double leftOffset, double rightOffset, double backOffset,
    okapi::RotationSensor RSL, double lDiameter,
    okapi::RotationSensor RSB, double bDiameter,
    pros::Imu compass
    ):
    offsetL(leftOffset), offsetR(rightOffset), offsetB(backOffset), 
    lastL(0), lastR(0), lastB(0), lastTheta(0), 
    x(0), y(0), theta(0), thetaOffset(0), 
    counter(0) ,
    rotationSensorL(RSL), lDiameter(lDiameter),
    rotationSensorB(RSB), bDiameter(bDiameter),
    IMU(compass) 
{
  //IMU.calibrate();
}

/* Getters */
double Odom::getX() {
    return x;
}
double Odom::getY() {
    return y;
}
double Odom::getTheta() {
    return theta/M_PI*180;
}

double Odom::getRotL() {
    return rotationSensorL.get();
}
double Odom::getRotB() {
    return rotationSensorB.get();
}
double Odom::getIMU() {
    return IMU.get_rotation();
}

void Odom::calibrateIMU() {
    IMU.reset();
}

void Odom::set(double x_0, double y_0, double theta_0) {
    x = x_0;
    y = y_0;

    thetaOffset = theta_0/180*M_PI - theta;
    theta = theta_0/180*M_PI;
    lastTheta += thetaOffset;
}



void Odom::update() {
    if(IMU.is_calibrating()) return;
    
    double l = getRotL()/360*M_PI*lDiameter;
    double r = 0; //No right odom wheel
    double b = getRotB()/360*M_PI*bDiameter;
    double t = getIMU()/180*M_PI + thetaOffset;
    if(counter < 5) {
        lastL = l;
        lastB = b;
        lastTheta = t;
    }
    if(isinf(t)) {
        // printf("odom iteration failed: theta is inf\n");
        return;
    }

    double adjustedTheta = t;
    double deltaL = l - lastL;
    double deltaR = r - lastR;
    double deltaB = b - lastB;
    double deltaTheta = (adjustedTheta)-lastTheta;

    if(fabs(deltaL) > 50 || fabs(deltaR) > 50 || fabs(deltaB) > 50) {
        printf("odom iteration failed: encoder change too great\n");
        return;
    }

    // double deltaX;
    // double deltaY;
    // if(deltaTheta == 0) {
    //     deltaX = deltaB;
    //     deltaY = deltaL;
    // } else {
    //     deltaX = 2*sin(deltaTheta/2) * ((deltaB/deltaTheta) + offsetB);
    //     deltaY = 2*sin(deltaTheta/2) * ((deltaL/deltaTheta) - offsetL);
    // }
    //
    // x += cos(lastTheta + deltaTheta/2)*deltaX - sin(lastTheta + deltaTheta/2)*deltaY;
    // y += sin(lastTheta + deltaTheta/2)*deltaX + cos(lastTheta + deltaTheta/2)*deltaY;
    // theta = (t/180*M_PI);
    //
    // lastR = r;
    // lastL = l;
    // lastB = b;
    // lastTheta = (t/180*M_PI);

  // const double deltaL =  / chassisScales.straight;
  // const double deltaR = itickDiff[1] / chassisScales.straight;

  // double deltaTheta = (deltaL - deltaR) / chassisScales.wheelTrack.convert(meter);
  double localOffX, localOffY;

  // const auto deltaM = static_cast<const double>(deltaB - (deltaTheta/2 * offsetB * 2));

  if (deltaTheta == 0) {
    localOffX = deltaB;
    localOffY = deltaL;
  } else {
    localOffX = 2*sin(deltaTheta/2)*(deltaB/deltaTheta + offsetB);
    localOffY = 2*sin(deltaTheta/2)*(deltaL/deltaTheta - offsetL);
  }

  double avgA = lastTheta + (deltaTheta / 2);

  // double polarR = std::sqrt((localOffX * localOffX) + (localOffY * localOffY));
  // double polarA = std::atan2(localOffY, localOffX) - avgA;
  //
  // double dX = std::sin(polarA) * polarR;
  // double dY = std::cos(polarA) * polarR;

  double dX = cos(avgA)*localOffX + sin(avgA)*localOffY;
  double dY = -sin(avgA)*localOffX + cos(avgA)*localOffY;

  if (isnan(dX)) {
    dX = 0;
  }

  if (isnan(dY)) {
    dY = 0;
  }

  if (isnan(deltaTheta)) {
    deltaTheta = 0;
  }

  x += dX;
  y += dY;
  theta = t;

  lastR = r;
  lastL = l;
  lastB = b;
  lastTheta = theta;
  

    // if(counter < 10) counter++;
    // else {
    //     counter = 0;
        // printf("%f %f %f %f %f %f %f %f\n", theta, deltaTheta, deltaL, deltaB, localOffX, localOffY, x, y);
    // }
    counter++;
}
