#include "pid.hpp"

#include "stdio.h"

using namespace std;

PID::PID(double kP, double kI, double kD, double threshold, int timeoutCount, double IThreshold, double DLimit, double min, double max):
    kP(kP), kI(kI), kD(kD), threshold(threshold), timeoutCount(timeoutCount), DLimit(DLimit),
    IThreshold(IThreshold), min(min), max(max), i(0), first(true), lastTarget(0), lastError(0), counter(0) {}

void PID::resetI() {
    i = 0;
}

void PID::resetD() {
    lastError = 0;
    lastTarget = 0;
    first = true;
}

double PID::calculate(double target, double input) {
    double error = target - input;
    if(first) {
        lastError = error;
        first = false;
    } else {
        lastError += target - lastTarget;
    }

    double p = error;
    fabs(error)<=IThreshold ? i+=error : i=0;
    double d = error - lastError;

    fabs(error)<=threshold ? counter++ : counter = 0;

    lastError = error;
    lastTarget = target;

    double output = kP*p + kI*i + kD*d;
    if(output > max) output=max;
    if(output < min) output=min;

    return output;
}

bool PID::isFinished() {
    return counter >= timeoutCount;
}

double PID::getError() {
    return lastError;
}
