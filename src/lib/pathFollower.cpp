#include "pathFollower.hpp"

using namespace std;

double distance(double x_0, double y_0, double x_1, double y_1) {
    return sqrt(pow(x_1-x_0, 2) + pow(y_1-y_0, 2));
}

PathFollower::PathFollower(double dt, double a_max, double wheelTrack, double straightLookaheadDistance):
    rate_max(a_max*dt), wheelTrack(wheelTrack), straightLookaheadDistance(straightLookaheadDistance), counter(0) {
  x = 0;
  y = 0;
  theta = 0;
  lookaheadDistance = 1;
  lookaheadCurvature = 0;
  targetVelocity = 0;
  closestPoint = 0;
  lookaheadIndex = 0;
  lookaheadPoint = {0, 0};
  finished = false;
}

void PathFollower::setPosition(double ax, double ay, double atheta) {
  x = ax;
  y = ay;
  theta = atheta;
}

void PathFollower::setPath(std::vector<myPoint> path) {
  currentPath = path;
  lookaheadDistance = 1;
  lookaheadCurvature = 0;
  targetVelocity = 0;
  closestPoint = 0;
  lookaheadIndex = 0;
  lookaheadPoint = {0, 0};
  finished = false;
}

void PathFollower::getClosestPoint() {
  int result = closestPoint;
  double runningMin = infinityf();
  // printf("-");
  for(int i = closestPoint; i<currentPath.size(); i++) {
    double adistance = distance(currentPath[i].x, currentPath[i].y, x, y);
    if(adistance < runningMin) {
      runningMin = adistance;
      // printf("%f\n", adistance);
      result = i;
    }
  }
  closestPoint = result;
  // printf("%d\n", currentPath.size());
  (closestPoint==currentPath.size()-1) ? counter++ : counter=0;
  // printf("%f %f\n", currentPath[closestPoint].x, currentPath[closestPoint].y);
}

void PathFollower::getLookahead() {
  // lookaheadDistance = 1/(1/straightLookaheadDistance + currentPath[closestPoint].curvature);
  lookaheadDistance = 1/(1/straightLookaheadDistance + currentPath.at(closestPoint).curvature);
  vector<double> result = lookaheadPoint;
  double fracIndex = lookaheadIndex;

  if(closestPoint==currentPath.size()-1) {
      lookaheadIndex = closestPoint;
      lookaheadPoint = {currentPath[closestPoint].x, currentPath[closestPoint].y};
      return;
  }

  for(int i=floor(lookaheadIndex); i<currentPath.size(); i++) {
    vector<double> d = {currentPath[i].x-currentPath[i-1].x, currentPath[i].y-currentPath[i-1].y};
    vector<double> f = {currentPath[i-1].x-x, currentPath[i-1].y-y};

    double a = d[0]*d[0] + d[1]*d[1];
    double b = 2*(f[0]*d[0] + f[1]*d[1]);
    double c = f[0]*f[0] + f[1]*f[1] - lookaheadDistance*lookaheadDistance;
    double discriminant = b*b - 4*a*c;
    if(discriminant < 0) continue;
    discriminant = sqrt(discriminant);
    double t = (-b - discriminant) / (2*a);

    if(t >= 0 && t <= 1) {
      if(i + t > fracIndex) {
        fracIndex = i + t;
        result = {currentPath[i].x + t*d[0], currentPath[i].y + t*d[1]};
      }
      continue;
    }
    t = (-b + discriminant) / (2*a);
    if(t >= 0 && t <= 1) {
      if(i + t > fracIndex) {
        fracIndex = i + t;
        result = {currentPath[i].x + t*d[0], currentPath[i].y + t*d[1]};
      }
    }
  }
  if(fracIndex>=currentPath.size()-1) {
      lookaheadIndex = currentPath.size()-1;
      lookaheadPoint = {currentPath[currentPath.size()-1].x, currentPath[currentPath.size()-1].y};
      return;
  }

  lookaheadIndex = fracIndex;
  lookaheadPoint = result;
}

void PathFollower::getLookaheadCurvature() {
  vector<double> lookaheadVector = {lookaheadPoint[0]-x, lookaheadPoint[1]-y};
  double tmpAngle = -theta/180*M_PI;
  double lookaheadX = cos(tmpAngle)*lookaheadVector[0] - sin(tmpAngle)*lookaheadVector[1];
  lookaheadCurvature = 2*lookaheadX / (lookaheadDistance*lookaheadDistance);
}

vector<double> PathFollower::getTargetVelocities() {
  getClosestPoint();
  getLookahead();
  getLookaheadCurvature();
  // double tmp = currentPath[closestPoint].velocity - targetVelocity;
  double tmp = currentPath.at(closestPoint).velocity-targetVelocity;
  if(tmp > 0) targetVelocity += min(tmp, rate_max);
  if(tmp < 0) targetVelocity += max(tmp, -rate_max);
  vector<double> result = {targetVelocity * (2+lookaheadCurvature*wheelTrack)/2, targetVelocity * (2-lookaheadCurvature*wheelTrack)/2};
  return result;
}

bool PathFollower::isSettled() {
  return counter >= 10;
}

double PathFollower::getClosestPointIndex() {
    return lookaheadIndex;
}

vector<double> PathFollower::getLookaheadPoint() {
    getClosestPoint();
    getLookahead();
    return lookaheadPoint;
}
