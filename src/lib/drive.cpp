#include "drive.hpp"

//#define _USE_MATH_DEFINES
using namespace okapi;
using namespace std;

Drive::Drive(int FLPort, int BLPort, int FRPort, int BRPort,
             bool FLRev, bool BLRev, bool FRRev, bool BRRev,
             AbstractMotor::gearset gearset,
             AbstractMotor::brakeMode brakeMode,
             AbstractMotor::encoderUnits encoderUnits):

    FLMotor (FLPort, FLRev, gearset, encoderUnits),
    BLMotor (BLPort, BLRev, gearset, encoderUnits),
    FRMotor (FRPort, FRRev, gearset, encoderUnits),
    BRMotor (BRPort, BRRev, gearset, encoderUnits)

{
   set_brake_mode(brakeMode);
};

void Drive::x_velocity(okapi::Controller controller, int maxVel) {
    double joyW = -controller.getAnalog(ControllerAnalog::rightX);
    double joyX = controller.getAnalog(ControllerAnalog::leftX);
    double joyY = controller.getAnalog(ControllerAnalog::leftY);
    double b = fabs(joyX) + fabs(joyY);
    double dist = sqrt(pow(joyX, 2) + pow(joyY, 2));
    double c = fabs(joyW) + dist;
    double C = fabs(joyW) + dist;

    if (c == 0) {
        move_voltage(0, 0);
        return;
    }/* else if (c > 1) {
        c = 1;
    }*/
    if (C > 1) {
        C = 1;
    }


    double w = (joyW / c) * C;
    double x = ((b == 0) ? 0 : (joyX / b) * (dist / c)) * C;
    double y = ((b == 0) ? 0 : (joyY / b) * (dist / c)) * C;

    FLMotor.moveVelocity(round((x + y - w) * maxVel));
    FRMotor.moveVelocity(round((-x + y + w) * maxVel));
    BLMotor.moveVelocity(round((-x + y - w) * maxVel));
    BRMotor.moveVelocity(round((x + y + w) * maxVel));
}

void Drive::x_voltage(okapi::Controller controller, int maxVol) {
    double joyW = -controller.getAnalog(ControllerAnalog::rightX);
    double joyX = controller.getAnalog(ControllerAnalog::leftX);
    double joyY = controller.getAnalog(ControllerAnalog::leftY);
    double b = fabs(joyX) + fabs(joyY);
    double dist = sqrt(pow(joyX, 2) + pow(joyY, 2));
    double c = fabs(joyW) + dist;
    double C = fabs(joyW) + dist;

    if (c == 0) {
        move_voltage(0, 0);
        return;
    }/* else if (c > 1) {
        c = 1;
    }*/
    if (C > 1) {
        C = 1;
    }


    double w = (joyW / c) * C;
    double x = ((b == 0) ? 0 : (joyX / b) * (dist / c)) * C;
    double y = ((b == 0) ? 0 : (joyY / b) * (dist / c)) * C;

    FLMotor.moveVoltage(round((x + y - w) * maxVol));
    FRMotor.moveVoltage(round((-x + y + w) * maxVol));
    BLMotor.moveVoltage(round((-x + y - w) * maxVol));
    BRMotor.moveVoltage(round((x + y + w) * maxVol));
}

void Drive::tank_velocity(okapi::Controller controller, int maxVel)
{
    int lVel = controller.getAnalog(ControllerAnalog::leftY)*maxVel;
    int rVel = controller.getAnalog(ControllerAnalog::rightY)*maxVel;

    move_velocity(lVel, rVel);
}


void Drive::tank_voltage(okapi::Controller controller, int maxVol)
{
    int lVol = controller.getAnalog(ControllerAnalog::leftY)*maxVol;
    int rVol = controller.getAnalog(ControllerAnalog::rightY)*maxVol;

    move_voltage(lVol, rVol);
}

void Drive::arcade_velocity(okapi::Controller controller, int maxVel)
{
    int lVel = (controller.getAnalog(ControllerAnalog::leftY)+controller.getAnalog(ControllerAnalog::rightX))*maxVel;
    int rVel = (controller.getAnalog(ControllerAnalog::leftY)-controller.getAnalog(ControllerAnalog::rightX))*maxVel;

    move_velocity(lVel, rVel);
}


void Drive::arcade_voltage(okapi::Controller controller, int maxVol)
{
    int lVol = (controller.getAnalog(ControllerAnalog::leftY)+controller.getAnalog(ControllerAnalog::rightX))*maxVol;
    int rVol = (controller.getAnalog(ControllerAnalog::leftY)-controller.getAnalog(ControllerAnalog::rightX))*maxVol;

    move_voltage(lVol, rVol);
}


void Drive::move_velocity(int lVel, int rVel)
{
    FLMotor.moveVelocity(lVel);
    BLMotor.moveVelocity(lVel);

    FRMotor.moveVelocity(rVel);
    BRMotor.moveVelocity(rVel);
}

void Drive::moveX_voltage(int flVol, int frVol, int blVol, int brVol) {
    FLMotor.moveVoltage(flVol);
    FRMotor.moveVoltage(frVol);
    BLMotor.moveVoltage(blVol);
    BRMotor.moveVoltage(brVol);
}

void Drive::move_voltage(int lVol, int rVol)
{
    FLMotor.moveVoltage(lVol);
    BLMotor.moveVoltage(lVol);

    FRMotor.moveVoltage(rVol);
    BRMotor.moveVoltage(rVol);
}


void Drive::set_brake_mode(AbstractMotor::brakeMode brakeMode)
{
    FLMotor.setBrakeMode(brakeMode);
    BLMotor.setBrakeMode(brakeMode);

    FRMotor.setBrakeMode(brakeMode);
    BRMotor.setBrakeMode(brakeMode);
}

vector<vector<double>> Drive::telemetry() {
    vector<vector<double>> tmp(4);
    vector<Motor> motors = {FLMotor, FRMotor, BLMotor, BRMotor};
    for(int i = 0; i < 4; i++) {
        tmp[i].push_back(motors[i].getPower());
    }
    return tmp;
}
