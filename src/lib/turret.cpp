#include "turret.hpp"

using namespace okapi::literals;

Turret::Turret(
        pros::Vision& camera,
        okapi::Potentiometer& pot, int potZero, double potAdjust,
        Odom& odom,
        PID& pid,
        okapi::Motor& motor,
        std::string color,
        double xTurretOffset, double yTurretOffset, double zTurretOffset, double thetaTurretOffset, double phiTurretOffset,
        double xCameraOffset, double yCameraOffset, double zCameraOffset, double thetaCameraOffset, double phiCameraOffset):
        camera(camera), pot(pot), potZero(potZero), potAdjust(potAdjust), odom(odom), pid(pid), motor(motor), red(color=="RED"), turretEnabled(false) {
    double alpha; double gamma;

    alpha = -thetaTurretOffset*M_PI/180;
    gamma = phiTurretOffset*M_PI/180;
    T_rt_0 =
        Eigen::Translation3d(xTurretOffset, yTurretOffset, zTurretOffset)*
        Eigen::AngleAxisd(alpha, Eigen::Vector3d(0, 0, 1))*
        Eigen::AngleAxisd(gamma, Eigen::Vector3d(1, 0, 0));

    alpha = -thetaCameraOffset*M_PI/180;
    gamma = phiCameraOffset*M_PI/180;
    T_tc =
        Eigen::Translation3d(xCameraOffset, yCameraOffset, zCameraOffset)*
        Eigen::AngleAxisd(alpha, Eigen::Vector3d(0, 0, 1))*
        Eigen::AngleAxisd(gamma, Eigen::Vector3d(1, 0, 0));
    
    // TODO: change this to reflect the object the camera will see and code aiming to aim above this point
    v_wg = {17.78, 17.78, 30.25};

    pros::Task aimTask(aim, (void*) this);
}


/*------------------------------------------------------------------------------*/
/*                                Observers                                     */
/*                                                                              */
/*------------------------------------------------------------------------------*/
Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> Turret::getT_wr() {
    double robotAngle = -odom.getTheta()*M_PI/180;
    Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> T_wr =
        Eigen::Translation3d(odom.getX(), odom.getY(), 0)*
        Eigen::AngleAxisd(robotAngle, Eigen::Vector3d::UnitZ());
    return T_wr;
}

Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> Turret::getT_rt() {
    double turretAngle = (pot.get()-2005)/4096*330*potAdjust*M_PI/180;
    Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> T_rt =
        T_rt_0 * Eigen::AngleAxisd(turretAngle, Eigen::Vector3d::UnitZ());
    return T_rt;
}

Eigen::Transform<double, 3, Eigen::TransformTraits::Isometry> Turret::getT_tc() {
    return T_tc;
}

bool Turret::getTurretEnabled() {
    return turretEnabled;
}

Eigen::Vector3d Turret::getv_wg() {
    v_wgMutex.take();
    auto tmp = v_wg;
    v_wgMutex.give();
    return tmp;
}

void Turret::readBySig(const std::uint32_t size_id, const std::uint32_t sig_id, const std::uint32_t object_count, pros::vision_object_s_t *const object_arr) {
    camera.read_by_sig(size_id, sig_id, object_count, object_arr);
}

double Turret::getError() {
    auto v_wg = getv_wg();
    // v_tg = T_tr*T_rw*v_wg subscript cancellation ftw
    auto v_tg = getT_rt().inverse()*getT_wr().inverse()*v_wg;
    Eigen::Vector3d v_tg_xy = {v_tg(0), v_tg(1), 0};
    return sgn(v_tg(0))*asin(Eigen::Vector3d::UnitY().cross(v_tg_xy.normalized()).norm());
}

bool Turret::goalInTurretRange() {
    auto v_wg = getv_wg();
    // v_tg = T_tr*T_rw*v_wg subscript cancellation ftw
    auto v_rg = getT_wr().inverse()*v_wg;
    Eigen::Vector3d v_rg_xy = {v_rg(0), v_rg(1), 0};
    double theta = sgn(v_rg(0)) * (v_rg(1)<0 ? M_PI - asin(Eigen::Vector3d::UnitY().cross(v_rg_xy.normalized()).norm()) : asin(Eigen::Vector3d::UnitY().cross(v_rg_xy.normalized()).norm()));
    return theta > Const::TURRET_LO_BOUND && theta < Const::TURRET_HI_BOUND;
}

/*------------------------------------------------------------------------------*/
/*                                Manipulators                                  */
/*                                                                              */
/*------------------------------------------------------------------------------*/
void Turret::setTurretEnabled(bool state) {
    turretEnabled = state;
}

void Turret::adjustGoal(Eigen::Vector3d adjustment) {
    v_wgMutex.take();
    v_wg += adjustment;
    v_wgMutex.give();
}

void Turret::turnVoltage(int vol) {
    motor.moveVoltage(vol);
}


/*------------------------------------------------------------------------------*/
/*                                Loop Methods                                  */
/*                                                                              */
/*------------------------------------------------------------------------------*/
void Turret::aim(void* arg) {
    okapi::Rate rate;
    auto obj = (Turret*) arg;
    while(true) {
        if(!obj->getTurretEnabled() || !obj->goalInTurretRange()) {
            obj->turnVoltage(0);
            rate.delay(10_Hz);
            continue;
        }
        
        obj->turnVoltage(obj->getError()*180/M_PI*150);
        rate.delay(10_Hz);
    }
    obj->turnVoltage(0);
}

void Turret::adjustGoalPos(void* arg) {
    okapi::Rate rate;
    auto obj = (Turret*) arg;
    while(true) {
        auto T_rt = obj->getT_rt();
        auto T_wr = obj->getT_wr();
        auto T_tc = obj->getT_tc();
        auto v_wg = obj->getv_wg();

        // v_cg = T_ct*T_tr*T_rw*v_wg
        auto v_cg = T_tc.inverse()*T_rt.inverse()*T_wr.inverse()*v_wg;
        if(v_cg(2) <= 0) {
            rate.delay(50_Hz);
            continue;
        }

        int box[4];
        int size[2];
        int xrange = v_cg(1)*tan(M_PI/6);
        int yrange = v_cg(1)*tan(M_PI/6*212/316);
        int center[2] = {
            (int)round(v_cg(0)/xrange*316/2),
            (int)round(v_cg(2)/yrange*212/2)};
        box[0] = round((v_cg(0)-15.1/2)/xrange * 316/2);
        box[1] = round((v_cg(2)-3.63/2)/yrange * 212/2);
        box[2] = round((v_cg(0)+15.1/2)/xrange * 316/2);
        box[3] = round((v_cg(2)+3.63/2)/yrange * 212/2);
        size[0] = round(15.1/xrange * 316/2);
        size[1] = round(3.63/yrange * 212/2);

        if(!((box[0]<-158)|(box[1]<-106)|(box[2]>158)|(box[3]>106))) {
            rate.delay(50_Hz);
            continue;
        }

        pros::vision_object_s_t blobs[20];
        obj->readBySig(0, 0, 20, blobs);
        double xSum = 0, ySum = 0, weight = 0;
        for(auto blob : blobs) {
            if(blob.signature == VISION_OBJECT_ERR_SIG) break;
            if(blob.width > size[0] || blob.height > size[1]) continue;
            if(blob.left_coord < box[0] || blob.top_coord < box[1] || blob.left_coord+blob.width > box[2] || blob.top_coord+blob.height > box[3]) continue;
            xSum += blob.x_middle_coord*blob.height*blob.width;
            ySum += blob.y_middle_coord*blob.height*blob.width;
            weight += blob.height*blob.width;
        }

        double average[2] = {xSum/weight, ySum/weight};
        double difference[2] = {average[0]-center[0], average[1]-center[1]};
        difference[0] *= Const::VISION_DAMPENING_FACTOR*xrange/316*2;
        difference[1] *= Const::VISION_DAMPENING_FACTOR*yrange/212*2;
        Eigen::Vector3d v_cadjust = {difference[0], difference[1], 0};
        // v_wadjust = T_wr*T_rt*T_tc*v_cadjust
        Eigen::Vector3d v_wadjust = T_wr*T_rt*T_tc*v_cadjust;
        obj->adjustGoal(v_wadjust);

        rate.delay(50_Hz);
    }
}

/**
 * @brief old broken code for reference only
 * 
 */
// // math maybe sus
// // Modern Robotics (3.22)
// // returns the reference frame of the vision sensor in the world frame
// Frame Turret::getf_wc() {
//     double x = odom.getX(), y = odom.getY(), theta = -odom.getTheta()*M_PI/180;
//     Frame f_wc;
//     Eigen::AngleAxisd r_wr(theta, Eigen::Vector3d(0, 0, 1));
//     f_wc.p = Eigen::Vector3d(x, y, 0) + r_wr*f_rc.p;
//     f_wc.R = r_wr*f_rc.R;
//     return f_wc;
// }

// Eigen::Vector3d Turret::getv_cg() {
//     Frame f_wc = getf_wc();
//     Eigen::Vector3d cg;
//     cg = f_wc.R.fullPivHouseholderQr().solve(v_wg - f_wc.p);
//     return cg;
// }

// bool Turret::goalInFrame(Eigen::Vector3d cg) {
//     Eigen::Vector4i pg;
//     if(cg(1) <= 0) {
//         return false;
//     }
//     int xrange = cg(1)*tan(M_PI/6);
//     int yrange = cg(1)*tan(M_PI/6*212/316);
//     pg(0) = round((cg(0)-15.1/2)/xrange * 316/2);
//     pg(1) = round((cg(2)-3.63/2)/yrange * 212/2);
//     pg(2) = round((cg(0)+15.1/2)/xrange * 316/2);
//     pg(3) = round((cg(2)+3.63/2)/yrange * 212/2);
//     return !((pg(0)<-158)|(pg(1)<-106)|(pg(2)>158)|(pg(3)>106));
// }

// Eigen::Vector4i Turret::getBox(Eigen::Vector3d cg) {
//     Eigen::Vector4i box;
//     int xrange = cg(1)*tan(M_PI/6);
//     int yrange = cg(1)*tan(M_PI/6*212/316);
//     box(0) = round((cg(0)-15.1/2)/xrange * 316/2);
//     box(1) = round((cg(2)-3.63/2)/yrange * 212/2);
//     box(2) = round((cg(0)+15.1/2)/xrange * 316/2);
//     box(3) = round((cg(2)+3.63/2)/yrange * 212/2);
//     return box;
// }

// Eigen::Vector2i Turret::getSize(Eigen::Vector3d cg) {
//     Eigen::Vector2i size;
//     int xrange = cg(1)*tan(M_PI/6);
//     int yrange = cg(1)*tan(M_PI/6*212/316);
//     size(0) = round((15.1)/xrange * 316/2);
//     size(1) = round((3.63)/yrange * 212/2);
//     return size;
// }

// // WIP
// void Turret::updateGoalPos() {
//     Eigen::Vector3d v_cg = getv_cg();
//     Eigen::Vector4i box = getBox(v_cg);
//     Eigen::Vector2i size = getSize(v_cg);
//     pros::vision_object_s_t blobs[20];
//     camera.read_by_sig(0, 0, 20, blobs);
//     for(auto blob : blobs) {
//         if(blob.signature == VISION_OBJECT_ERR_SIG || blob.width > size(0) || blob.height > size(1)) break;

//     }
// }

// Eigen::Vector3d Turret::getv_rg() {
//     double theta = -odom.getTheta()*M_PI/180;
//     Eigen::Vector3d v_wr = {odom.getX(), odom.getY(), 0};
//     Eigen::AngleAxisd r_wr(theta, Eigen::Vector3d(0, 0, 1));
//     return r_wr.inverse()*(v_wg-v_wr);
// }

// bool Turret::getTurretState() {
//     return turretEnabled;
// }

// PID Turret::getPID() {
//     return pid;
// }

// void Turret::setTurretState(bool state) {
//     turretEnabled = state;
//     if(turretEnabled) {
//         std::pair<okapi::Motor&, Turret*> argPair(motor, this);
//         std::pair<okapi::Motor&, Turret*>* arg = &argPair;
//         pros::Task pidTask(pidFunction, (void*) arg);
//     }
// }

// void Turret::pidFunction(void* arg) {
//     using namespace okapi::literals;
//     okapi::Rate rate;
//     std::pair<okapi::Motor&, Turret*>* argPair = (std::pair<okapi::Motor&, Turret*>*) arg;
//     while(argPair->second->getTurretState()) {
//         Eigen::Vector3d v_rg = argPair->second->getv_rg();
//         double theta = (v_rg(0)==0 && v_rg(1)==0 ? 0 : acos(v_rg(0)/ sqrt(v_rg(0)*v_rg(0)+v_rg(1)*v_rg(1)) ));
//         // argPair->first.moveVoltage(argPair->second->getPID().calculate(0, theta));
//         argPair->first.moveVoltage((M_PI_2-theta)/M_PI_2*12000);
//         rate.delay(100_Hz);
//     }
//     argPair->first.moveVoltage(0);
// }
