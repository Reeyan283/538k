#include "main.h"
#include "intake.hpp"
#include "pros/rtos.hpp"
#include <typeinfo>
using namespace okapi::literals;

Intake::Intake(int motorA_port, int motorB_port, 
               bool motorA_rev, bool motorB_rev,
               okapi::AbstractMotor::gearset gearset, okapi::AbstractMotor::brakeMode brakeMode, okapi::AbstractMotor::encoderUnits encoderUnits, okapi::Controller controller,
               okapi::ControllerDigital forwardButton,
               okapi::ControllerDigital reverseButton,
               okapi::ControllerDigital rollerButton):
    motorA (motorA_port, motorA_rev, gearset, encoderUnits),
    motorB (motorB_port, motorB_rev, gearset, encoderUnits),

    fBtn(controller[forwardButton]),
    rBtn(controller[reverseButton]),
    rlBtn(controller[rollerButton])
{
    setBrakeMode(brakeMode);

    running = false;
    enabled = false;
    overriding = false;


    //pros::Task intakeTask(run, (void*) this);
}

void Intake::drive(okapi::Controller controller, int maxVol, int rolVel)
{   
    if(fBtn.changedToPressed()) {
        std::printf("hello");
        overriding = false;
        running = (running == 1 ? 0 : 1);
        state = running;
    } else if(rlBtn.isPressed()) {
        overriding = false;
        state = (rBtn.isPressed() ? 2 : -2);
    } else if(rBtn.isPressed()) {
        overriding = false;
        state = -1;
    } else if(!overriding) {
        state = running;
    }
}

void Intake::run(void* args) {
    auto obj = (Intake*) args;
    okapi::Rate rate;
    while(true) {
        switch(obj->getState()) {
            case -3:
                obj->moveVoltage(-7000);
                break;
            case -2:
                obj->moveVelocity(-400);
                break;
            case -1:
                obj->moveVoltage(-12000);
                break;
            case 0:
                obj->moveVoltage(0);
                break;
            case 1:
                obj->moveVoltage(12000);
                break;
            case 2:
                obj->moveVelocity(400);
                break;
            default:
                break;
        }
        rate.delay(100_Hz);
    }
}

void Intake::setState(int astate) {
    state = astate;
    overriding = true;
    if(state == 1) running = 1;
}

int Intake::getState() {
    return state;
}

void Intake::setEnabled(bool enable) {
    enabled = enable;
}

bool Intake::getEnabled() {
    return enabled;
}

double Intake::getTorque() {
    return (motorA.getTorque() + motorB.getTorque()) / 2;
}

void Intake::setBrakeMode(okapi::AbstractMotor::brakeMode brakeMode)
{
    motorA.setBrakeMode(brakeMode);
    motorB.setBrakeMode(brakeMode);
}

void Intake::moveVoltage(int vol) 
{
    motorA.moveVoltage(vol);
    motorB.moveVoltage(vol);
}

void Intake::moveVelocity(int vel) 
{
    motorA.moveVelocity(vel);
    motorB.moveVelocity(vel);
}
