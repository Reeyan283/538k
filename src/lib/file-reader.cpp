#include "file-reader.hpp"

using namespace std;

vector<myPoint> readPointCSV(string filepath) {
  ifstream file(filepath);
  vector<myPoint> values {};
  string line;

  while(getline(file, line)) {
    double dtmp;
    vector<double> tmp;
    stringstream ss(line);
    while(ss >> dtmp) {
      string tmpstr;
      tmp.push_back(dtmp);
      if(!getline(ss, tmpstr, ',')) break;
    }
    myPoint ptmp(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4]);
    values.push_back(ptmp);
  }
  return values;
}
