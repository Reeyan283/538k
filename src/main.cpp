#include "main.h"

using namespace okapi;
using namespace std;

// coordinate origin is at the corner where your goal is.

Controller controller;

Drive drive(Const::DRIVE_FL_ID,    Const::DRIVE_BL_ID,    Const::DRIVE_FR_ID,    Const::DRIVE_BR_ID,
            Const::DRIVE_FL_REV, Const::DRIVE_BL_REV, Const::DRIVE_FR_REV, Const::DRIVE_BR_REV,
            AbstractMotor::gearset::green,
            AbstractMotor::brakeMode::coast,
            AbstractMotor::encoderUnits::degrees);

Intake intake(Const::INTAKE_A_ID, Const::INTAKE_B_ID,
              Const::INTAKE_A_REV, Const::INTAKE_B_REV,
              AbstractMotor::gearset::blue,
              AbstractMotor::brakeMode::coast,
              AbstractMotor::encoderUnits::degrees,
              controller,
              Const::INTAKE_FWD_BTN,
              Const::INTAKE_REV_BTN,
              Const::INTAKE_RLR_BTN);

okapi::RotationSensor rotL(Const::ODOM_L_ID, Const::ODOM_L_REV);
okapi::RotationSensor rotB(Const::ODOM_B_ID, Const::ODOM_B_REV);
pros::Imu inertial(Const::ODOM_IMU_ID);

pros::ADIDigitalOut indexer (Const::INDEXER_ID);
Motor flyMotor(Const::FLY_ID, Const::FLY_REV, AbstractMotor::gearset::blue, AbstractMotor::encoderUnits::degrees);
Motor turretMotor(2, true, AbstractMotor::gearset::green, AbstractMotor::encoderUnits::degrees);

Odom odom(
    7.40625, 1.5625, 1.5625,
    rotL, Const::ODOM_L_DIA,
    rotB, Const::ODOM_B_DIA,
    inertial
    );

pros::Vision camera(Const::VISION_ID, pros::E_VISION_ZERO_CENTER);

Potentiometer turretPot('B');



PID drivePID(1200, 30, 800, 1, 10, 5, 10000, -12000, 12000);
PID turnPID(240000, 0, 0, 0.001, 10, infinityf(), 10000, -12000, 12000);

PID drivePID2(1200, 30, 800, 1, 10, 5, 10000, -12000, 12000);
PID turnPID2(240000, 0, 0, 0.001, 10, infinityf(), 10000, -12000, 12000);

PID turnPID3(300, 25, 0, 0.25, 50, 5, 10000, -12000, 12000);

Turret turret(
    camera,
    turretPot, 2005, 90/80,
    odom,
    drivePID,
    turretMotor,
    "RED",
    0, 0, 0, 0, 0,
    0, 0, 0, 0, 0);

atomic<bool> auton = false;
atomic<double> targetX = 0;
atomic<double> targetY = 0;
atomic<bool> settled = true;
atomic<bool> strafeSettled = true;
atomic<bool> turnSettled = true;

void logFunction() {
    Rate logRate;
    while(true) {
        // printf("%f %f %f\n", odom.getX(), odom.getY(), odom.getTheta());
        // printf("%f\n", ((double) turretPot.get()-1905)/4096*330*90/80);
        // printf("%f\n", intake.getTorque());
        // printf("%f %f\n", perpendicular.get(), parallel.get());
        // printf("%f %f %f %f %d\n", odom.getX(), odom.getY(), purePursuit.getTargetVelocities()[0], purePursuit.getTargetVelocities()[1], purePursuit.getClosestPointIndex());
        // printf("%f %f\n", purePursuit.getTargetVelocities()[0], purePursuit.getTargetVelocities()[1]);
        // vector<vector<double>> tmp = drive.telemetry(); 
        // cout << tmp[0][0] << " " << tmp[1][0] << " " << tmp[2][0] << " " << tmp[3][0] << "\n";
        logRate.delay(10_Hz);
    }
}

void drivePIDFunction() {
    Rate drivePIDRate;
    bool stopped = true;
    bool turnDone = false;
    settled = false;
    
    do {
        vector<double> error = {targetX-odom.getX(), targetY-odom.getY()};
        vector<double> xVector = {cos(-odom.getTheta()/180*M_PI), sin(-odom.getTheta()/180*M_PI)};
        vector<double> yVector = {cos(-odom.getTheta()/180*M_PI+M_PI/2), sin(-odom.getTheta()/180*M_PI+M_PI/2)};
        double distance = sqrt(error[0]*error[0] + error[1]*error[1]);
        // projections, only need scalar and the vector being projected onto has a norm of 1
        double xProjScalar = error[0]*xVector[0] + error[1]*xVector[1];
        double yProjScalar = error[0]*yVector[0] + error[1]*yVector[1];
        double curvature = (distance>1) ? 2*xProjScalar/pow(distance,2) : 0;

        int straightVol = round(drivePID.calculate(0, -yProjScalar));
        int turnVol = round(turnPID.calculate(0, -curvature));
        // int turnVol = 0;
        // printf("%f\n", drivePID.getError());
        // printf("%f %f %f %f %f %f %f %f\n", xVector[0], xVector[1], yVector[0], yVector[1], xProjScalar, yProjScalar, distance, odom.getTheta());

        (distance>6) ? drive.move_voltage(straightVol+turnVol, straightVol-turnVol) : drive.move_voltage(straightVol, straightVol);
        if(distance < 6) turnDone = true;
        else turnDone = false;

        drivePIDRate.delay(100_Hz);
    } while(auton && (!drivePID.isFinished() || !(turnPID.isFinished() || turnDone)));
    settled = true;
    drive.move_voltage(0, 0);
}

void strafePIDFunction() {
    Rate strafePIDRate;
    bool stopped = true;
    bool turnDone = false;
    strafeSettled = false;
    do {
        vector<double> error = {targetX-odom.getX(), targetY-odom.getY()};
        vector<double> xVector = {cos(-odom.getTheta()/180*M_PI), sin(-odom.getTheta()/180*M_PI)};
        vector<double> yVector = {cos(-odom.getTheta()/180*M_PI+M_PI/2), sin(-odom.getTheta()/180*M_PI+M_PI/2)};
        double distance = sqrt(error[0]*error[0] + error[1]*error[1]);
        // projections, only need scalar and the vector being projected onto has a norm of 1
        double xProjScalar = error[0]*xVector[0] + error[1]*xVector[1];
        double yProjScalar = error[0]*yVector[0] + error[1]*yVector[1];
        double curvature = (distance>1) ? -2*yProjScalar/pow(distance,2) : 0;

        int straightVol = round(drivePID2.calculate(0, -xProjScalar));
        int turnVol = round(turnPID2.calculate(0, -curvature));
        // int turnVol = 0;
        // printf("%f\n", drivePID.getError());
        // printf("%f %f %f %f %f %f %f %f\n", xVector[0], xVector[1], yVector[0], yVector[1], xProjScalar, yProjScalar, distance, odom.getTheta());

        (distance>6) ? drive.moveX_voltage(straightVol+turnVol, -straightVol-turnVol, -straightVol+turnVol, straightVol-turnVol) : drive.moveX_voltage(straightVol, -straightVol, -straightVol, straightVol);
        if(distance < 6) turnDone = true;
        else turnDone = false;

        strafePIDRate.delay(100_Hz);
    } while(auton && (!drivePID2.isFinished() || !(turnPID2.isFinished() || turnDone)));
    strafeSettled = true;
    drive.move_voltage(0, 0);
}

void turnPIDFunction() {
    Rate turnPIDRate;
    bool stopped = true;
    bool turnDone = false;
    turnSettled = false;
    do {
        vector<double> error = {targetX-odom.getX(), targetY-odom.getY()};
        vector<double> xVector = {cos(-odom.getTheta()/180*M_PI), sin(-odom.getTheta()/180*M_PI)};
        vector<double> yVector = {cos(-odom.getTheta()/180*M_PI+M_PI/2), sin(-odom.getTheta()/180*M_PI+M_PI/2)};
        double distance = sqrt(error[0]*error[0] + error[1]*error[1]);
        // projections, only need scalar and the vector being projected onto has a norm of 1
        double xProjScalar = error[0]*xVector[0] + error[1]*xVector[1];
        double yProjScalar = error[0]*yVector[0] + error[1]*yVector[1];
        // double curvature = (distance>1) ? 2*xProjScalar/pow(distance,2) : 0;
        double angle = 180/M_PI*atan(-xProjScalar/yProjScalar);

        // int straightVol = round(drivePID2.calculate(0, -yProjScalar));
        int straightVol = 0;
        int turnVol = round(turnPID3.calculate(0, angle));
        // int turnVol = 0;
        // printf("%f\n", drivePID.getError());
        // printf("%f %f %f %f %f %f %f %f\n", xVector[0], xVector[1], yVector[0], yVector[1], xProjScalar, yProjScalar, distance, odom.getTheta());

        drive.moveX_voltage(straightVol+turnVol, -straightVol-turnVol, -straightVol+turnVol, straightVol-turnVol);
        // if(distance < 6) turnDone = true;
        // else turnDone = false;

        turnPIDRate.delay(100_Hz);
    } while(auton && (!(turnPID3.isFinished() || turnDone)));
    turnSettled = true;
    drive.move_voltage(0, 0);
}

void driveTo(double x, double y) {
    Rate rate;
    targetX = x;
    targetY = y;
    settled = false;
    pros::Task drivePIDTask(drivePIDFunction);
    while(!settled) rate.delay(20_Hz);
}

void strafeTo(double x, double y) {
    Rate rate;
    targetX = x;
    targetY = y;
    strafeSettled = false;
    pros::Task strafePIDTask(strafePIDFunction);
    while(!strafeSettled) rate.delay(20_Hz);
}

void turnTo(double x, double y) {
    Rate rate;
    targetX = x;
    targetY = y;
    turnSettled = false;
    pros::Task turnPIDTask(turnPIDFunction);
    while(!turnSettled) rate.delay(20_Hz);
}


void odomFunction() {
    Rate odomRate;
    while(true) {
        odom.update();
        odomRate.delay(100_Hz);
    }
}

void on_center_button() {}

void initialize() {
    Rate initRate;
    
    
    while(inertial.is_calibrating()) {
        initRate.delay(5_Hz);
    }
    // odom.set(12, 36, -90);
    pros::Task logTask(logFunction);
    pros::Task odomTask(odomFunction);
    odom.set(144-11.75, 32.75, -90);

    pros::Task intakeTask(intake.run, (void*) &intake);
}

void disabled() {
    auton = false;
}

void competition_initialize() {}

void autonomous() {
    
    odom.set(144-11.75, 32.75, -90);
    auton = true;
    Rate rate;
    while(inertial.is_calibrating()) {
        rate.delay(5_Hz);
    }
    Timer timer;
	Controller controller;

    // targetX = 24;
    // targetY = 24;
    // pros::Task drivePIDTask(drivePIDFunction);
    // while(!settled) rate.delay(100_Hz);

    // turnTo(11.75, 32.75);
    // timer.placeMark();
    // while(timer.getDtFromMark().convert(millisecond) < 1000) rate.delay(100_Hz);
    // driveTo(11.75, 32.75);

    timer.placeMark();
    drive.move_voltage(-4000, -4000);
    intake.moveVoltage(-6000);
    while(timer.getDtFromMark().convert(millisecond) < 700) rate.delay(100_Hz);
    drive.move_voltage(0, 0);
    intake.moveVoltage(0);
    driveTo(odom.getX(), 14);
    strafeTo(74, odom.getY());
    // drive.move_voltage(12000,12000);
    // timer.placeMark();
    // while(timer.getDtFromMark().convert(millisecond) < 1000) rate.delay(100_Hz);
    intake.moveVoltage(12000);
    driveTo(83.5, 60.5);
    driveTo(78.5, 65.5);
    flyMotor.moveVoltage(11500);
    
    turnTo(25.5, 126.22);
    drive.move_voltage(0, 0);
    intake.moveVoltage(0);

    Timer indexTimer;
    indexTimer.placeMark();
    timer.placeMark();
    
    int counter = 0;
    while(auton) {
        // double rpm = flyMotor.getActualVelocity()*6;
        // double error = target - rpm;
        // output += error * gain;
        // if(output > 12000) output = 12000;
        // if(output < -12000) output = -12000;
        // if(std::signbit(error) != std::signbit(lastError)) {
        //     output = (output + tbh) / 2; 
        //     tbh = output;
        // }
        // lastError = error;
        // if(output > 12000) output = 12000;
        // if(output < -12000) output = -12000;
        flyMotor.moveVoltage(11500);

        if(indexTimer.getDtFromMark().convert(millisecond) > 1000 && flyMotor.getActualVelocity() > 500 && counter < 3) {
            indexTimer.placeMark();
            indexer.set_value(true);
            counter++;
        } else if(indexTimer.getDtFromMark().convert(millisecond) > 100) {
            indexer.set_value(false);
        }
        if(counter >= 3) break;
    }
    timer.placeMark();
    while(timer.getDtFromMark().convert(millisecond) < 200) rate.delay(100_Hz);

}

void opcontrol() {
    odom.set(144-11.75, 32.75, -90);
    auton = false;
    // turret.setTurretEnabled(true);
    // pros::Task logTask(logFunction);
    Rate rate;
    Timer indexTimer;
    Timer flyTimer;
    Timer jamTimer;
    flyTimer.placeMark();
    indexTimer.placeMark();
    jamTimer.placeMark();
    Controller controller;
    bool intakeStopped = true;
    bool intakeFlyLatch = false;
    bool flyLatch = false;
    bool flyState = 0;
    bool ejectDone = true;
    bool jammed = false;
    bool reversing = false;
    bool intakeRising = false;
    bool flyFlag = false;
    int lastIntakeState = 0;

    int counter = 0;
	double tbh = 9000;
	double gain = 0.25;
	double output = 0;
	double target = 3000;
	// double lastError = target - fly.getActualVelocity()*5/2;
	double lastError = target - flyMotor.getActualVelocity()*6;
    rate.delayUntil(1000);
    odom.set(144-11.75, 32.75, -90);
    turret.setTurretEnabled(true);
	while (true) {
        // drive.x_velocity(controller, 200);
        if(ejectDone && controller.getDigital(ControllerDigital::up)) {
            // intake.setState(false);
            ejectDone = false;
            target = 900;
            flyState = true;
        } else if(ejectDone && !flyState && controller.getDigital(ControllerDigital::down)) {
            reversing = true;
            target = -1200;
            flyState = true;
        } else if(ejectDone && controller.getDigital(ControllerDigital::down)) {
            target = -1200;
        } else if(ejectDone && reversing && !controller.getDigital(ControllerDigital::down)) {
            reversing = false;
            flyState = false;
        } else if(ejectDone) {
            target = 3000;
        }

        double rpm = flyMotor.getActualVelocity()*6;
        double error = target - rpm;
        if(flyState) {
            flyFlag = true;
            // intake.setState(false);
            output += error * gain;
            if(output > 12000) output = 12000;
            if(output < -12000) output = -12000;
            if(std::signbit(error) != std::signbit(lastError)) {
                output = (output + tbh) / 2;
                tbh = output;
            }
            lastError = error;
            if(output > 12000) output = 12000;
            if(output < -12000) output = -12000;
            flyMotor.moveVoltage(output);
        } else if(intakeRising) {
            flyMotor.moveVoltage(-1000);
            flyFlag = false;
        } else if(flyFlag) {
            flyMotor.moveVoltage(0);
        }

        if(!ejectDone) {
            if(flyMotor.getActualVelocity()*6 > 700 && flyMotor.getActualVelocity()*6 < 1100) {
                indexTimer.placeMark();
                indexer.set_value(true);
                ejectDone = true;
                target = 3000;
                flyState = false;
            }
        }

        
        // if(!jammed && )
        // if(!jammed && intake.getState() == 1 && intake.getTorque() > 0.34) {
        //     jamTimer.placeMark();
        //     intake.setState(-1);
        //     jammed = true;
        // }

        // if(jammed && jamTimer.getDtFromMark().convert(millisecond) > 800) {
        //     intake.setState(1);
        //     jammed = false;
        // }

        if(!intakeStopped && flyTimer.getDtFromMark().convert(millisecond) > 2000) {
            intake.setState(0);
            intakeStopped = true;
        }

        if(indexTimer.getDtFromMark().convert(millisecond) > 600 && fabs(error) < 400 && controller.getDigital(ControllerDigital::L1)) {
            indexTimer.placeMark();
            indexer.set_value(1);
        } else if(indexTimer.getDtFromMark().convert(millisecond) > 100) {
            indexer.set_value(0);
        }

        //std::printf("%d", controller[Const::INTAKE_FWD_BTN].isPressed());
        intake.drive(controller, 12000, 225);
        if(intake.getState() == 1 && lastIntakeState == 0) {
            intakeRising = true;
        } else {
            intakeRising = false;
        }

        lastIntakeState = intake.getState();

        if (!intakeFlyLatch && controller.getDigital(Const::INTAKE_FWD_BTN)) {
            flyState = false;
            intakeFlyLatch = true;
        } else if (intakeFlyLatch && !controller.getDigital(Const::INTAKE_FWD_BTN)) {
            intakeFlyLatch = false;
        }
        drive.x_voltage(controller, 12000);
        if(!flyLatch && controller.getDigital(ControllerDigital::R2)) {
            flyLatch = true;
            flyState = !flyState;
            if(flyState) {
                flyTimer.placeMark();
                intakeStopped = false;
            }
        } else if(flyLatch && !controller.getDigital(ControllerDigital::R2)) {
            flyLatch = false;
        }

        

		rate.delay(100_Hz);
	}
    
		
}
